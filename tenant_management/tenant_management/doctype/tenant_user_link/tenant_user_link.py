# Copyright (c) 2023, Castlecraft Ecommerce Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


class TenantUserlink(Document):
    def before_insert(self):
        frappe_user = frappe.db.get_value(
            "Tenant User", self.tenant_user, "user"
        )  # noqa: E501
        permission_exist = frappe.db.exists(
            "User Permission",
            {"user": frappe_user, "allow": "Tenant", "for_value": self.tenant},
        )
        if not permission_exist:
            user_permission = frappe.new_doc("User Permission")
            user_permission.user = frappe_user
            user_permission.allow = "Tenant"
            user_permission.for_value = self.tenant
            user_permission.save()
            frappe.db.commit()

    def on_trash(self):
        frappe_user = frappe.db.get_value(
            "Tenant User", self.tenant_user, "user"
        )  # noqa: E501
        permission_exist = frappe.db.exists(
            "User Permission",
            {"user": frappe_user, "allow": "Tenant", "for_value": self.tenant},
        )
        if permission_exist:
            frappe.delete_doc("User Permission", permission_exist)
            frappe.db.commit()
