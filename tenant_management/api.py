import frappe


@frappe.whitelist()
def add_tenant_user(tenant_user, user=None):
    if not user:
        user = frappe.session.user
    exist = frappe.db.exists(
        "Tenant User", {"user_name": tenant_user, "user": user}
    )  # noqa: E501
    if exist:
        frappe.throw(tenant_user + " is already linked with the user " + user)
    else:
        new_user = frappe.new_doc("Tenant User")
        new_user.user_name = tenant_user
        new_user.user = user
        new_user.save()
        frappe.db.commit()


@frappe.whitelist()
def remove_tenant_user(tenant_user, user):
    exist = frappe.db.exists(
        "Tenant User", {"user_name": tenant_user, "user": user}
    )  # noqa: E501
    if not exist:
        frappe.throw(tenant_user + " is not aailable in the system")
    else:
        frappe.delete_doc("Tenant User", tenant_user)
        frappe.db.commit()


@frappe.whitelist()
def update_role(tenant, tenant_user, frappe_roles=None, tenant_role=None):
    exist = frappe.db.exists(
        "Tenant User link",
        {
            "tenant_user": tenant_user,
            "tenant": tenant,
            "tenant_role": tenant_role,
        },  # noqa: E501
    )
    if not exist:
        frappe.throw("No record found for " + tenant + " " + tenant_user)

    else:
        role_link = frappe.get_last_doc(
            "Tenant User link",
            filters={
                "tenant_user": tenant_user,
                "tenant": tenant,
                "tenant_role": tenant_role,
            },
        )
        role_link.tenant_role = tenant_role
        role_link.save()
        frappe.db.commit()


@frappe.whitelist()
def get_tenants(user=None):
    if not user:
        user = frappe.session.user
    return frappe.get_all("Tenant User", filters={"user", user})


@frappe.whitelist()
def get_tenant_users(tenant):
    return frappe.get_all(
        "Tenant User link", filters={"tenant", tenant}, fields=["tenant_user"]
    )


@frappe.whitelist()
def validate_tenant_role(
    tenant, user=None, allowed_roles=None
):  # allowed_roles will be list of roles
    if not user:
        user = frappe.session.user
        tenant_user = frappe.db.get_value(
            "Tenant User", {"user": user}, "user_name"
        )  # noqa: E501
    else:
        tenant_user = user

    if not allowed_roles:
        allowed_roles = []

    exist = frappe.db.exists(
        "Tenant User link",
        {
            "tenant_user": tenant_user,
            "tenant": tenant,
            "tenant_role": ["in", allowed_roles],
        },
    )

    if not exist:
        frappe.throw(tenant_user + " dont have permission for " + tenant)
